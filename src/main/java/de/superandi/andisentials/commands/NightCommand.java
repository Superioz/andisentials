package de.superandi.andisentials.commands;

import de.superandi.andisentials.io.LanguageManager;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class NightCommand implements CommandExecutor {

    private static final Integer NIGHT_TIME = 16000;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof ConsoleCommandSender) {
            sender.sendMessage(LanguageManager.get("no-console-command"));
            return true;
        }

        Player p = (Player) sender;
        p.getWorld().setTime(NIGHT_TIME);
        p.playSound(p.getLocation(), Sound.GLASS, 1, 1);
        p.sendMessage(LanguageManager.get("command-night-success", NIGHT_TIME));
        return true;
    }

}
package de.superandi.andisentials.commands;

import de.superandi.andisentials.io.LanguageManager;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class RainCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if(sender instanceof ConsoleCommandSender) {
            sender.sendMessage(LanguageManager.get("no-console-command"));
            return true;
        }

        Player p = (Player) sender;
        p.getWorld().setStorm(true);
        p.playSound(p.getLocation(), Sound.GLASS, 1, 1);
        p.sendMessage(LanguageManager.get("command-rain-success"));
        return true;
    }


}

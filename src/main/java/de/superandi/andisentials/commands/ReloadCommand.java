package de.superandi.andisentials.commands;

import de.superandi.andisentials.AndiSentials;
import de.superandi.andisentials.io.LanguageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ReloadCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        AndiSentials.getInstance().onReload();
        sender.sendMessage(LanguageManager.get("command-reload-success"));
        return true;
    }
}

package de.superandi.andisentials.commands;

import de.superandi.andisentials.AndiSentials;
import de.superandi.andisentials.io.LanguageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by test on 23.08.2017.
 */
public class SpeedCommand implements CommandExecutor {


    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof ConsoleCommandSender) {
            sender.sendMessage(LanguageManager.get("no-console-command"));
            return true;
        }

        Player p = (Player) sender;
        if (args.length == 0 || args.length > 2) {
            // wrong usage
            p.sendMessage(LanguageManager.get("command-speed-only"));
        } else if (args.length == 1) {
            // speed only with duration, no amplifier
            try {
                int duration = Integer.parseInt(args[0]);
                applyEffect(p, duration * 20, 0);
            } catch (Exception ex) {
                p.sendMessage(LanguageManager.get("command-speed-words"));
            }
        } else if (args.length == 2) {
            // speed with duration and amplifier

            try {
                int duration = Integer.parseInt(args[0]);
                int amplifier = Integer.parseInt(args[1]);

                applyEffect(p, duration * 20, amplifier - 1);
            } catch (Exception ex) {
                p.sendMessage(LanguageManager.get("command-speed-words-all"));
            }
        }
        return true;
    }

    private void applyEffect(Player p, int duration, int amplifier) {
        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, duration, amplifier));

        // send message
        p.sendMessage(LanguageManager.get("command-speed-succesful"));
    }


}

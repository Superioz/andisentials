package de.superandi.andisentials.commands;

import de.superandi.andisentials.common.SpawnManager;
import de.superandi.andisentials.io.LanguageManager;
import de.superandi.andisentials.utils.LocationSerializer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class SetSpawnCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(args.length == 4) {
            // the player wants to set the position via coords
            String xString = args[0], yString = args[1], zString = args[2], world = args[3];

            try {
                double x = Double.parseDouble(xString);
                double y = Double.parseDouble(yString);
                double z = Double.parseDouble(zString);

                Location loc = new Location(Bukkit.getWorld(world), x, y, z);
                SpawnManager.getInstance().setSpawn(loc);
                sender.sendMessage(LanguageManager.get("command-setspawn-success", LocationSerializer.simpleSerializeLocation(loc)));
            }
            catch(NumberFormatException ex) {
                sender.sendMessage(LanguageManager.get("command-setspawn-error"));
            }
            return true;
        }
        if(sender instanceof ConsoleCommandSender) {
            sender.sendMessage(LanguageManager.get("command-setspawn-no-console"));
            return true;
        }
        Player p = (Player) sender;
        Location loc = p.getLocation();

        SpawnManager.getInstance().setSpawn(loc);
        p.playSound(p.getLocation(), Sound.GLASS, 1, 1);
        p.sendMessage(LanguageManager.get("command-setspawn-success", LocationSerializer.simpleSerializeLocation(loc)));
        return true;
    }


}

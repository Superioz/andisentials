package de.superandi.andisentials.commands;

import de.superandi.andisentials.io.LanguageManager;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class SunCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if(sender instanceof ConsoleCommandSender) {
            sender.sendMessage(LanguageManager.get("no-console-command"));
            return true;
        }

        Player p = (Player) sender;
        World w = p.getWorld();

        w.setStorm(false);
        w.setThundering(false);
        p.playSound(p.getLocation(), Sound.GLASS, 1, 1);
        p.sendMessage(LanguageManager.get("command-sun-success"));
        return true;
    }


}

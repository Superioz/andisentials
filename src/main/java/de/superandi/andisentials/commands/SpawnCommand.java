package de.superandi.andisentials.commands;

import de.superandi.andisentials.common.SpawnManager;
import de.superandi.andisentials.io.LanguageManager;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class SpawnCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(sender instanceof ConsoleCommandSender) {
            sender.sendMessage(LanguageManager.get("no-console-command"));
            return true;
        }

        Player p = (Player) sender;
        boolean worldSpawnFlag = Arrays.asList(args).contains("-w");
        Location spawn = worldSpawnFlag ? p.getWorld().getSpawnLocation() : SpawnManager.getInstance().getSpawn();

        // if the spawn is null - couldnt load
        if(spawn == null) {
            p.sendMessage(LanguageManager.get("command-spawn-null"));
            return true;
        }

        p.teleport(spawn.add(0, .5, 0));
        p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
        p.sendMessage(LanguageManager.get(worldSpawnFlag ? "command-worldspawn-success" : "command-spawn-success"));
        return true;
    }

}
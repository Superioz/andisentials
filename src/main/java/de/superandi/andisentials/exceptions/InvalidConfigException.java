package de.superandi.andisentials.exceptions;

import de.superandi.andisentials.io.JsonConfig;
import lombok.Getter;

/**
 * Exception if something from the config couldn't be found
 *
 * @see JsonConfig
 */
public class InvalidConfigException extends RuntimeException {

    @Getter
    private JsonConfig config;

    public InvalidConfigException(String message, JsonConfig config) {
        super(message);
        this.config = config;
    }

}

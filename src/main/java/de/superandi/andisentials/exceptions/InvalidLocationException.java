package de.superandi.andisentials.exceptions;

import lombok.Getter;

/**
 * Exception if a serialized location is forced to be deserialized but there is an error
 */
public class InvalidLocationException extends RuntimeException {

    @Getter
    private String serializedLocation;

    public InvalidLocationException(String message, String serializedLocation) {
        super(message);
        this.serializedLocation = serializedLocation;
    }

}

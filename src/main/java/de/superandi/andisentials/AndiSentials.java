package de.superandi.andisentials;

import de.superandi.andisentials.commands.*;
import de.superandi.andisentials.io.JsonConfig;
import de.superandi.andisentials.io.LanguageManager;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class AndiSentials extends JavaPlugin {

    private static final String LANGUAGE_FILE = "language";
    private static final String COMMON_BACKUP_FILE = "common_backup";

    // instance
    private static AndiSentials instance;

    public static synchronized AndiSentials getInstance() {
        if(instance == null) {
            instance = new AndiSentials();
        }
        return instance;
    }

    // other instances
    private LanguageManager languageManager;
    private JsonConfig commonBackup;

    public static String Prefix = "&7[&cAndiSentials&7] |";


    @Override
    public void onEnable() {
        instance = this;

        // registers the commands
        getLogger().info("Register commands ..");
        this.getCommand("day").setExecutor(new DayCommand());
        this.getCommand("night").setExecutor(new NightCommand());
        this.getCommand("sun").setExecutor(new SunCommand());
        this.getCommand("rain").setExecutor(new RainCommand());
        this.getCommand("spawn").setExecutor(new SpawnCommand());
        this.getCommand("setspawn").setExecutor(new SetSpawnCommand());
        this.getCommand("asreload").setExecutor(new ReloadCommand());
        this.getCommand("speed").setExecutor(new SpeedCommand());

        // get language.properties
        getLogger().info("Load files ..");
        languageManager = new LanguageManager(LANGUAGE_FILE, getDataFolder(),
                s -> ChatColor.translateAlternateColorCodes('&', s));
        commonBackup = new JsonConfig(COMMON_BACKUP_FILE, getDataFolder());
        commonBackup.load(true, true);

    }

    @Override
    public void onDisable() {

    }

    /**
     * If the user uses the reload command, execute this command
     */
    public void onReload() {
        if(languageManager != null) languageManager.reload();
        if(commonBackup != null) commonBackup.load(true, true);
    }

}

package de.superandi.andisentials.utils;

import de.superandi.andisentials.exceptions.InvalidLocationException;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.regex.Pattern;

public class LocationSerializer {

    private static final String SPACER = "#";
    private static final Pattern SERIALIZED_LOC_PATTERN = Pattern.compile("([^ ]*#){5}[^ ]*");

    /**
     * Serializes the location
     *
     * @param loc The location
     * @return The serialized loc
     */
    public static String serializeLocation(Location loc) {
        return loc.getX() + SPACER + loc.getY() + SPACER + loc.getZ()
                + SPACER + loc.getYaw() + SPACER + loc.getPitch() + SPACER + loc.getWorld().getName();
    }

    public static String simpleSerializeLocation(Location loc) {
        return "X:" + loc.getBlockX() + ",Y:" + loc.getBlockY() + ",Z:" + loc.getBlockZ();
    }

    /**
     * Deserializes given location string
     *
     * @param serializedLoc The location as string
     * @return The location
     * @throws InvalidLocationException If the string is wrong formatted
     */
    public static Location deserializeLocation(String serializedLoc) throws InvalidLocationException {
        if(!SERIALIZED_LOC_PATTERN.matcher(serializedLoc).matches()) {
            throw new InvalidLocationException("The location is formatted wrongly!", serializedLoc);
        }
        String[] split = serializedLoc.split(SPACER);

        try {
            double x = Double.parseDouble(split[0]);
            double y = Double.parseDouble(split[1]);
            double z = Double.parseDouble(split[2]);
            float yaw = Float.parseFloat(split[3]);
            float pitch = Float.parseFloat(split[4]);
            World world = Bukkit.getWorld(split[5]);

            return new Location(world, x, y, z, yaw, pitch);
        }
        catch(Exception e) {
            throw new InvalidLocationException("Couldn't deserialize location!", serializedLoc);
        }
    }

}

package de.superandi.andisentials.common;

import de.superandi.andisentials.AndiSentials;
import de.superandi.andisentials.exceptions.InvalidLocationException;
import de.superandi.andisentials.utils.LocationSerializer;
import org.bukkit.Location;

public class SpawnManager {

    private static final String SPAWN_CONFIG_KEY = "spawn";

    private static SpawnManager instance;

    public static synchronized SpawnManager getInstance() {
        if(instance == null) {
            instance = new SpawnManager();
        }
        return instance;
    }

    /**
     * Sets the spawnpoint by serializing the location and setting it into the file backup
     *
     * @param loc The location
     */
    public void setSpawn(Location loc) {
        AndiSentials.getInstance().getCommonBackup().set(SPAWN_CONFIG_KEY, LocationSerializer.serializeLocation(loc));
    }

    /**
     * Simply returns the spawn location out of the file
     *
     * @return The location
     */
    public Location getSpawn() {
        String deserializedSpawn = AndiSentials.getInstance().getCommonBackup().get(SPAWN_CONFIG_KEY);

        try {
            return LocationSerializer.deserializeLocation(deserializedSpawn);
        }
        catch(InvalidLocationException ex) {
            //
        }
        return null;
    }

}
